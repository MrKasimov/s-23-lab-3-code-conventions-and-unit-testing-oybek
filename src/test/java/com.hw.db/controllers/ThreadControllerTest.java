package com.hw.db.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import com.hw.db.models.Thread;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

class ThreadControllerTest {

    private final String username = "username";
    private final int id = 11111;
    private final String slug = "slug";
    private final Thread thread = new Thread(username, Timestamp.from(Instant.now()), "forum", "message", slug, "title", 0);
    private final Post post = new Post(username, Timestamp.from(Instant.now()), "forum", "message", 0, 0, true);
    private final User user = new User(username, "email", "fullname", "about");

    private final ThreadController threadController = new ThreadController();

    @BeforeEach
    void setUp() {
        thread.setId(id);
    }

    @Test
    @DisplayName("thread retrieval test")
    void checkIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(id))
                    .thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                    .thenReturn(thread);
            Assertions.assertEquals(thread, threadController.CheckIdOrSlug(Integer.toString(id)));
            Assertions.assertEquals(thread, threadController.CheckIdOrSlug(slug));
        }
    }

    @Test
    @DisplayName("post creation test")
    void createPost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadById(id))
                        .thenReturn(thread);
                userMock.when(() -> UserDAO.Info(username))
                        .thenReturn(user);
                ResponseEntity responseEntity = threadController.createPost(Integer.toString(id), singletonList(post));
                Assertions.assertEquals(CREATED, responseEntity.getStatusCode());
                Assertions.assertEquals(singletonList(post), responseEntity.getBody());
            }
        }
    }

    @Test
    @DisplayName("posts retrieval test")
    void posts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            List<Post> posts = singletonList(post);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                    .thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getPosts(id, 1, 2000, "-", true))
                    .thenReturn(posts);
            ResponseEntity responseEntity = threadController.Posts(slug, 1, 2000, "-", true);
            Assertions.assertEquals(OK, responseEntity.getStatusCode());
            Assertions.assertEquals(posts, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("thread change test")
    void change() {
        Thread thread2 =
                new Thread(username, new Timestamp(0), "forum", "message", "another slug", "title", 0);
        thread2.setId(22222);
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(22222))
                    .thenReturn(thread2);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("another slug"))
                    .thenReturn(thread2);
            ResponseEntity responseEntity = threadController.change("another slug", thread2);
            Assertions.assertEquals(OK, responseEntity.getStatusCode());
            Assertions.assertEquals(thread2, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("thread info test")
    void info() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                    .thenReturn(thread);
            ResponseEntity responseEntity = threadController.info(slug);
            Assertions.assertEquals(OK, responseEntity.getStatusCode());
            Assertions.assertEquals(thread, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("vote creation test")
    void createVote() {
        Vote vote = new Vote(username, 1);
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                        .thenReturn(thread);
                userMock.when(() -> UserDAO.Info(username))
                        .thenReturn(user);
                ResponseEntity responseEntity = threadController.createVote(slug, vote);
                Assertions.assertEquals(OK, responseEntity.getStatusCode());
                Assertions.assertEquals(thread, responseEntity.getBody());
            }
        }
    }
}
